﻿using EmployeeRegister.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeRegister.Data
{
    public class AppDBContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmergencyContact> EmergencyContacts { get; set; }

        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            SetupEmployee(modelBuilder);
        }

        private void SetupEmployee(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                .HasKey(x => x.EmployeeId);

            modelBuilder.Entity<Employee>()
                .HasIndex(e => e.EmailAddress)
                .IsUnique(true);

            modelBuilder.Entity<Employee>()
                .Property(e => e.EmailAddress)
                .IsRequired()
                .HasMaxLength(255);

            modelBuilder.Entity<Employee>()
                       .HasOne(e => e.EmergencyContact)
                       .WithOne(c => c.Employee)
                       .HasForeignKey<EmergencyContact>(c => c.EmployeeRef);
        }
    }
}
