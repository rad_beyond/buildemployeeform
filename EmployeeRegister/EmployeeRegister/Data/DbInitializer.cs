﻿using EmployeeRegister.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeRegister.Data
{
    public static class DbInitializer 
    {
        public static void Seed(AppDBContext context)
        {
            if (!context.Employees.Any())
            {
                context.Employees.AddRange(new Employee
                {
                    Address = "Albany Auckland",
                    FirstName = "Jhon",
                    LastName = "Doe",
                    PhoneNo = "0221094889",
                    EmailAddress = "JhonDoe@gmail.com",
                    EmployeeType = EmployeeType.Developer,
                    IsSameAsFullAddress = true,
                    StartDate = DateTime.UtcNow,
                    CitizenShipStatus = "NZ",
                    PositionTitle = "Manager",
                    EmergencyContact = new EmergencyContact
                    {
                        EmergencyContactId = 1,
                        Name = "Marry",
                        Relationship = "Wife",
                        PhoneNo = "0234567890"
                    }
                });
            }

            context.SaveChanges();
        }
    }
}
