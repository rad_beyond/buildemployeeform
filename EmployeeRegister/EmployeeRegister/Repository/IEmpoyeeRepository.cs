﻿using EmployeeRegister.Models;
using System.Collections.Generic;

namespace EmployeeRegister
{
    public interface IEmpoyeeRepository
    {
        IEnumerable<Employee> GetAllEmployees();
        void CreateEmployee(Employee employee);
    }
}