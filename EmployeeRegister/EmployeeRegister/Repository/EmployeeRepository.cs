﻿using System.Collections.Generic;
using System.Linq;
using EmployeeRegister.Data;
using EmployeeRegister.Models;
using Microsoft.EntityFrameworkCore;

namespace EmployeeRegister
{
    public class EmployeeRepository : IEmpoyeeRepository
    {
        private AppDBContext _appDbContext;

        public EmployeeRepository(AppDBContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public void CreateEmployee(Employee employee)
        {
            employee.DocumentId = new System.Guid();
            _appDbContext.Employees.Add(employee);
            _appDbContext.SaveChanges();
        }

        public IEnumerable<Employee> GetAllEmployees()
        {
            return _appDbContext.Employees.Include(x =>x.EmergencyContact).ToList();
        }

    }
}