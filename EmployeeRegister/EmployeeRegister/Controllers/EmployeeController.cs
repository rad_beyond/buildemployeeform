﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EmployeeRegister.Models;
using EmployeeRegister.ViewModels;
using System.IO;
using System.Text;

namespace EmployeeRegister.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IEmpoyeeRepository _employeeRepository;

        public EmployeeController(IEmpoyeeRepository empoyeeRepository)
        {
            _employeeRepository = empoyeeRepository;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Employee Overview";
            var employees = _employeeRepository.GetAllEmployees().OrderBy(e => e.EmployeeId);

            return View(employees);
        }

        [HttpGet]
        public IActionResult AddEmployee()
        {
            var employeeEditViewModel = new EditEmployeeViewModel
            {
            };

            return View(employeeEditViewModel);
        }

        [HttpPost]
        public IActionResult AddEmployee(EditEmployeeViewModel employeeEditViewModel)
        {
          
            if (ModelState.IsValid)
            {
                _employeeRepository.CreateEmployee(employeeEditViewModel.Employee);
                //return RedirectToAction("Index");
            }

            return View(employeeEditViewModel);
        }

        [HttpGet]
        public IActionResult GetCsv()
        {
            var employees = _employeeRepository.GetAllEmployees().OrderBy(e => e.EmployeeId);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Employee Id, Employee Name, Employee Email, Emloyee PhoneNo, Employee Position, Employee Start Date, Emergency Contact Name,  Emergency Contact PhoneNo");
            foreach(var e in employees)
            {
                sb.AppendLine(string.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}", 
                             e.EmployeeId, e.FirstName + e.LastName, e.EmailAddress, e.PhoneNo, e.PositionTitle, e.StartDate, e.EmergencyContact.Name, e.EmergencyContact.PhoneNo));
       
            }

            return File(System.Text.Encoding.ASCII.GetBytes(sb.ToString()), "text/csv", "data.csv");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
