﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [Employees] (
    [DocumentId] uniqueidentifier NOT NULL,
    [EmployeeId] int NOT NULL IDENTITY,
    [FirstName] nvarchar(100) NOT NULL,
    [LastName] nvarchar(100) NOT NULL,
    [Address] nvarchar(max) NOT NULL,
    [IsSameAsFullAddress] bit NOT NULL,
    [EmailAddress] nvarchar(255) NOT NULL,
    [PhoneNo] nvarchar(max) NOT NULL,
    [CitizenShipStatus] nvarchar(max) NULL,
    [StartDate] datetime2 NOT NULL,
    [EmployeeType] int NOT NULL,
    [PositionTitle] nvarchar(max) NULL,
    CONSTRAINT [PK_Employees] PRIMARY KEY ([EmployeeId])
);

GO

CREATE TABLE [EmergencyContacts] (
    [EmergencyContactId] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NULL,
    [Relationship] nvarchar(max) NULL,
    [PhoneNo] nvarchar(max) NOT NULL,
    [EmployeeRef] int NOT NULL,
    CONSTRAINT [PK_EmergencyContacts] PRIMARY KEY ([EmergencyContactId]),
    CONSTRAINT [FK_EmergencyContacts_Employees_EmployeeRef] FOREIGN KEY ([EmployeeRef]) REFERENCES [Employees] ([EmployeeId]) ON DELETE CASCADE
);

GO

CREATE UNIQUE INDEX [IX_EmergencyContacts_EmployeeRef] ON [EmergencyContacts] ([EmployeeRef]);

GO

CREATE UNIQUE INDEX [IX_Employees_EmailAddress] ON [Employees] ([EmailAddress]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20191112074925_ting', N'2.1.8-servicing-32085');

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20191112075512_initialCommit', N'2.1.8-servicing-32085');

GO

