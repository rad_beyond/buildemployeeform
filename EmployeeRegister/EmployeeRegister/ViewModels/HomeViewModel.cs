﻿using EmployeeRegister.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeRegister.ViewModels
{
    public class HomeViewModel
    {
        public string Title { get; set; }

        public IEnumerable<Employee> Employees { get; set; }
    }
}
