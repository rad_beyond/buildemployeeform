﻿using EmployeeRegister.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeRegister.ViewModels
{
    public class EditEmployeeViewModel
    {
        public Employee Employee { get; set; }
    }
}
