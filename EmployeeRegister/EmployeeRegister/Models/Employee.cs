﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmployeeRegister.Models
{
    public class Employee
    {

        public Guid DocumentId { get; set; }

        public int EmployeeId { get; set; }

        [Required]
        [Display(Name = "First Name:")]
        [StringLength(100, ErrorMessage = "your first name is required")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name:")]
        [StringLength(100, ErrorMessage = "your last name is required")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Full Address:")]
        public string Address  { get; set; }

        [Required]
        [Display(Name = "Mailing Address:")]
        public bool IsSameAsFullAddress { get; set; }

        [Required]
        [Display(Name = "Email Address:")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|""(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])",
          ErrorMessage = "The email address is not entered in a correct format")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "You must provide a phone number")]
        [Display(Name = "Phone No:")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string PhoneNo { get; set; }

        [Display(Name = "Citizenship Statas:")]
        public string CitizenShipStatus { get; set; }

  
        [Display(Name = "Employee Start Date:")]
        [DataType(DataType.Date)] // todo. check if the start date has to be future date.
        public DateTime StartDate { get; set; }

        [Display(Name = "Employee Type:")]
        public EmployeeType EmployeeType { get; set; }

        [Display(Name = "Position Title:")]
        public string PositionTitle { get; set; }

        public virtual EmergencyContact EmergencyContact { get; set; }
    }
}