﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace EmployeeRegister.Models
{
    public enum EmployeeType
    {
        Developer,
        Tester,
        Manager
    }
}
