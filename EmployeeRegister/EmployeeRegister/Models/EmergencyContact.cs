﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeRegister.Models
{
    public class EmergencyContact
    {
        public int EmergencyContactId { get; set; }

        [Display(Name = "Name:")]
        public string Name { get; set; }

        [Display(Name = "Relationship:")]
        public string Relationship { get; set; }

        [Required(ErrorMessage = "You must provide a phone number")]
        [Display(Name = "Phone No:")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string PhoneNo { get; set; }

        public int EmployeeRef { get; set; }

        public Employee Employee { get; set;}
    }
}
